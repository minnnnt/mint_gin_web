package entity

import (
	"time"
)

type PlayerProfile struct {
	Id         int       `gorm:"-;primary_key;AUTO_INCREMENT"`
	Account    string    `json:"account"`
	Password   string    `json:"password"`
	Email      string    `json:"email"`
	Age        int8      `json:"age"`
	CreateTime time.Time `json:"create_time"`
}
