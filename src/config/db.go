package config

import (
	"drink_a_glass_of_gin/src/common/entity"
	"github.com/spf13/viper"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"log"
)

func Init() *gorm.DB {
	url := readConfig("DB_URL")
	db, err := gorm.Open(postgres.Open(url), &gorm.Config{})
	log.Println("End connect DB: ", url)

	if err != nil {
		log.Fatalln(err)
	}

	db.AutoMigrate(&entity.PlayerProfile{})
	log.Println("DB AutoMigrate.")
	return db
}

func readConfig(configKey string) string {
	viper.SetConfigFile("./src/common/envs/.env")
	viper.ReadInConfig()
	viper.AutomaticEnv()

	c := viper.Get(configKey).(string)
	return c
}
