package main

import (
	"drink_a_glass_of_gin/src/config"
	"drink_a_glass_of_gin/src/player"
	"github.com/gin-gonic/gin"
)

func main() {

	r := gin.Default()
	db := config.Init()

	player.RegisterRoutes(r, db)

	r.Run("localhost:3333")
}
