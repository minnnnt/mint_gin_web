package player

import (
	"drink_a_glass_of_gin/src/common/entity"
	"github.com/gin-gonic/gin"
	"net/http"
)

type AddPlayerReq struct {
	Account  string `json:"account"`
	Password string `json:"password"`
	Email    string `json:"email"`
	Age      int8   `json:"age"`
}

func (h handler) AddPlayer(c *gin.Context) {
	body := AddPlayerReq{}

	// getting request's body
	if err := c.BindJSON(&body); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	var player entity.PlayerProfile

	player.Account = body.Account
	player.Password = body.Password
	player.Email = body.Email
	player.Age = body.Age

	if result := h.DB.Create(&player); result.Error != nil {
		c.AbortWithError(http.StatusNotFound, result.Error)
		return
	}

	c.JSON(http.StatusCreated, &player)
}
