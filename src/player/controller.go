package player

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type handler struct {
	DB *gorm.DB
}

func RegisterRoutes(r *gin.Engine, db *gorm.DB) {
	h := &handler{
		DB: db,
	}

	routes := r.Group("/player")
	routes.POST("/", h.AddPlayer)
	routes.GET("/:id", h.GetPlayers)
	routes.PUT("/:id", h.UpdatePlayer)

}
