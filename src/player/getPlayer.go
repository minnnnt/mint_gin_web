package player

import (
	"drink_a_glass_of_gin/src/common/entity"
	"net/http"

	"github.com/gin-gonic/gin"
)

func (h handler) GetPlayer(c *gin.Context) {
	id := c.Param("id")

	var player entity.PlayerProfile

	if result := h.DB.First(&player, id); result.Error != nil {
		c.AbortWithError(http.StatusNotFound, result.Error)
		return
	}

	c.JSON(http.StatusOK, &player)
}
