package player

import (
	"drink_a_glass_of_gin/src/common/entity"
	"net/http"

	"github.com/gin-gonic/gin"
)

func (h handler) GetPlayers(c *gin.Context) {
	var players []entity.PlayerProfile

	if result := h.DB.Find(&players); result.Error != nil {
		c.AbortWithError(http.StatusNotFound, result.Error)
		return
	}

	c.JSON(http.StatusOK, &players)
}
