package player

import (
	"drink_a_glass_of_gin/src/common/entity"
	"github.com/gin-gonic/gin"
	"net/http"
)

type UpdatePlayerReq struct {
	Account  string `json:"account"`
	Password string `json:"password"`
	Email    string `json:"email"`
	Age      int8   `json:"age"`
}

func (h handler) UpdatePlayer(c *gin.Context) {
	id := c.Param("id")
	body := UpdatePlayerReq{}

	// getting request's body
	if err := c.BindJSON(&body); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	var player entity.PlayerProfile

	if result := h.DB.First(&player, id); result.Error != nil {
		c.AbortWithError(http.StatusNotFound, result.Error)
		return
	}

	player.Password = body.Password
	player.Email = body.Email
	player.Age = body.Age

	h.DB.Save(&player)

	c.JSON(http.StatusOK, &player)
}
